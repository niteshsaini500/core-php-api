<?php 
include_once 'database.php';
include_once 'Company.php';

$database = new Database();
$db = $database->getConnection();
$list = new Company($db);
$result = $list->getData();
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Data Lists</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="css/style.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <!--  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Records</a>
      </li>
    </ul>
  </nav> -->
<!-- 
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block">Dashboard</a>
        </div>
      </div>
    </div>
  </aside> -->

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Records</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Records</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Company Data</h3>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>API Date</th>
                    <th>API Time</th>
                    <th>Company Name</th>
                    <th>Plant</th>
                    <th>GSTIN</th>
                    <th>Bill Pkey</th>
                    <th>CE Flag</th>
                    <th>Doc Type</th>
                    <th>Created At</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if (!empty($result)) {  
                    foreach ($result as $key => $value) { ?>
                      <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['api_date']; ?></td>
                        <td><?php echo $value['api_time']; ?></td>
                        <td><?php echo $value['company_name']; ?></td>
                        <td><?php echo $value['plant']; ?></td>
                        <td><?php echo $value['gstin']; ?></td>
                        <td><?php echo $value['bill_pkey']; ?></td>
                        <td><?php echo $value['ce_flag']; ?></td>
                        <td><?php echo $value['doc_type']; ?></td>
                        <td><?php echo date("D, d M Y g:i:s", strtotime($value['created_at'])); ?></td>
                      </tr>
                    <?php } }else{ ?>
                      <tr>
                        <td class="text-center">Data Empty.</td>
                      </tr>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>API Date</th>
                    <th>API Time</th>
                    <th>Company Name</th>
                    <th>Plant</th>
                    <th>GSTIN</th>
                    <th>Bill Pkey</th>
                    <th>CE Flag</th>
                    <th>Doc Type</th>
                    <th>Created At</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.1.0-rc
    </div>
    <strong>Copyright &copy; 2020-2021 <!-- <a href="#">example.com</a>. --></strong> All rights reserved.
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/dataTables.responsive.min.js"></script>
<script src="js/responsive.bootstrap4.min.js"></script>
<script src="js/dataTables.buttons.min.js"></script>
<script src="js/buttons.bootstrap4.min.js"></script>
<script src="js/jszip.min.js"></script>
<script src="js/pdfmake.min.js"></script>
<script src="js/vfs_fonts.js"></script>
<script src="js/buttons.html5.min.js"></script>
<script src="js/buttons.print.min.js"></script>
<script src="js/buttons.colVis.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>


<!-- 
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Data</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h4>Data Listing</h4>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>Api Date</th>
        <th>Api Time</th>
        <th>Company Name</th>
        <th>Plant</th>
        <th>GSTIN</th>
        <th>Bill Pkey</th>
        <th>CE Flag</th>
        <th>Doc Type</th>
        <th>Created at</th>
      </tr>
    </thead>
    <tbody>
      <?php if (!empty($result)) {  
        foreach ($result as $key => $value) { ?>
          <tr>
            <td><?php echo $value['id']; ?></td>
            <td><?php echo $value['api_date']; ?></td>
            <td><?php echo $value['api_time']; ?></td>
            <td><?php echo $value['company_name']; ?></td>
            <td><?php echo $value['plant']; ?></td>
            <td><?php echo $value['gstin']; ?></td>
            <td><?php echo $value['bill_pkey']; ?></td>
            <td><?php echo $value['ce_flag']; ?></td>
            <td><?php echo $value['doc_type']; ?></td>
            <td><?php echo     $newDate = date("D, d M Y g:i:s", strtotime($value['created_at'])); ?></td>
          </tr>
        <?php } }else{ ?>
          <tr>
            <td class="text-center">Data Empty.</td>
          </tr>
        <?php } ?>
    </tbody>
  </table>
</div>

</body>
</html> -->