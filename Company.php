<?php

Class Company{
	private $conn;
    private $table_name = "company";
 
    public $id;
	public $api_date;
	public $api_time;
	public $company_name;
	public $plant;
	public $gstin;
	public $bill_pkey;
	public $ce_flag;
	public $doc_type;

    public function __construct($db){
        $this->conn = $db;
    }


    function create(){

        $query = "INSERT INTO " . $this->table_name . "
                SET
                    api_date = :api_date,
                    api_time = :api_time,
                    company_name = :company_name,
                    plant = :plant,
                    gstin = :gstin,
                    bill_pkey = :bill_pkey,
                    ce_flag = :ce_flag,
                    doc_type = :doc_type";
     
        $stmt = $this->conn->prepare($query);
     
        // $this->name=htmlspecialchars(strip_tags($this->name));
     
        $stmt->bindParam(':api_date', $this->api_date);
        $stmt->bindParam(':api_time', $this->api_time);
        $stmt->bindParam(':company_name', $this->company_name);
        $stmt->bindParam(':plant', $this->plant);
        $stmt->bindParam(':gstin', $this->gstin);
        $stmt->bindParam(':bill_pkey', $this->bill_pkey);
        $stmt->bindParam(':ce_flag', $this->ce_flag);
        $stmt->bindParam(':doc_type', $this->doc_type);

        $result = $stmt->execute();
        if($result){
            return true;
        }
        return false;
    }


    function getData(){

        $query = "SELECT * FROM " . $this->table_name; 
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $num = $stmt->rowCount();
        
        if($num>0){
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        return false;
    }
}
?>