-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 22, 2020 at 07:42 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apis`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `api_date` date NOT NULL,
  `api_time` time NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `plant` varchar(255) NOT NULL,
  `gstin` varchar(255) NOT NULL,
  `bill_pkey` varchar(255) NOT NULL,
  `ce_flag` varchar(255) NOT NULL,
  `doc_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `flag` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `api_date`, `api_time`, `company_name`, `plant`, `gstin`, `bill_pkey`, `ce_flag`, `doc_type`, `created_at`, `updated_at`, `flag`) VALUES
(1, '2020-12-15', '05:08:10', 'ABC', 'plant', 'qwertyuiopqwert', 'qwertyuiopqwertyui', 'AB', 'qwertyuiop', '2020-12-22 22:44:21', '0000-00-00 00:00:00', 1),
(2, '2020-12-15', '05:08:10', 'ABC', 'plant', 'qwertyuiopqwert', 'qwertyuiopqwertyui', 'AB', 'qwertyuiop', '2020-12-22 22:46:25', '0000-00-00 00:00:00', 1),
(3, '2020-12-15', '05:08:10', 'ABCa3rerwfgsdvq3', 'plant', 'qwertyuiopqwert', 'qwertyuiopqwertyui', 'AB', 'qwertyuiop', '2020-12-22 23:18:15', '0000-00-00 00:00:00', 1),
(4, '2020-12-15', '05:08:10', 'ABCa3rerwfgsdvq3', 'plant', 'qwertyuiopqwert', 'qwertyuiopqwertyui', 'AB', 'qwertyuiop', '2020-12-22 23:18:30', '0000-00-00 00:00:00', 1),
(5, '2020-12-15', '05:08:10', 'ABCa3rerwfgsdvq3', 'plant', 'qwertyuiopqwert', 'qwertyuiopqwertyui', 'AB', 'qwertyuiop', '2020-12-22 23:18:39', '0000-00-00 00:00:00', 1),
(6, '2020-12-15', '05:08:10', 'ABCa3rerwfgsdvq3', 'plant', 'qwertyuiopqwert', 'qwertyuiopqwertyui', 'AB', 'qwertyuio', '2020-12-22 23:18:48', '0000-00-00 00:00:00', 1),
(7, '2020-12-15', '05:08:10', 'ABCa3rerwfgsdvq3', 'plant', 'qwertyuiopqwert', 'qwertyuiopqwertyui', 'AB', 'qwertyuioi', '2020-12-22 23:18:52', '0000-00-00 00:00:00', 1),
(8, '2020-12-15', '05:08:10', 'ABC', 'plant', 'QWERTYUIO', 'qwertyuiopqwertyui', 'AB', 'qwertyuioi', '2020-12-22 23:19:15', '0000-00-00 00:00:00', 1),
(9, '2020-12-15', '05:08:10', 'ABC', 'plant', 'QWERTYUIO', 'qwertyuiopqwertyui', 'AB', 'qwertyuioi', '2020-12-23 00:06:59', '0000-00-00 00:00:00', 1),
(10, '2020-12-15', '05:08:10', 'ABC', 'plant', 'QWERTYUIO', 'qwertyuiopqwertyui', 'AB', 'qwertyuioi', '2020-12-23 00:07:00', '0000-00-00 00:00:00', 1),
(11, '2020-12-15', '05:08:10', 'ABC', 'plant', 'QWERTYUIO', 'qwertyuiopqwertyui', 'AB', 'qwertyuioi', '2020-12-23 00:07:00', '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
