<?php 
include_once 'database.php';
include_once 'Company.php';

$postedData = json_decode(file_get_contents("php://input"));

if(
  isset($postedData->api_date) && 
  isset($postedData->api_time) && 
  isset($postedData->company_name) && 
  isset($postedData->plant) && 
  isset($postedData->gstin) && 
  isset($postedData->bill_pkey) && 
  isset($postedData->ce_flag) && 
  isset($postedData->doc_type)
){

	$api_date 		= $postedData->api_date;
	$api_time 		= $postedData->api_time;
	$company_name 	= trim($postedData->company_name);
	$plant 			= trim($postedData->plant);
	$gstin 			= trim($postedData->gstin);
	$bill_pkey 		= trim($postedData->bill_pkey);
	$ce_flag 		= trim($postedData->ce_flag);
	$doc_type 		= trim($postedData->doc_type);


	if(empty($api_date)){
		echo json_encode(array(
      		"message" => "Enter your api date !."
		));exit();
   	}else if(empty($api_time)){
		echo json_encode(array(
      		"message" => "Enter your api time !."
		));exit();
   	}else if(empty($company_name)){
		echo json_encode(array(
      		"message" => "Enter your company name !."
		));exit();
   	}else if(empty($plant)){
		echo json_encode(array(
      		"message" => "Enter your plant !."
		));exit();
   	}else if(empty($gstin)){
		echo json_encode(array(
      		"message" => "Enter your gstin !."
		));exit();
   	}else if(empty($bill_pkey)){
		echo json_encode(array(
      		"message" => "Enter your bill pkey !."
		));exit();
   	}else if(empty($ce_flag)){
		echo json_encode(array(
      		"message" => "Enter your ce flag !."
		));exit();
   	}else if(empty($doc_type)){
		echo json_encode(array(
      		"message" => "Enter your doc type !."
		));exit();
   	}else if(strlen($company_name) > 150){
      echo json_encode(array(
          "message" => "Company name length exceeded more than 150 character !."
    ));exit();
    }else if(strlen($plant) > 6){
      echo json_encode(array(
          "message" => "plant length exceeded more than 6 character !."
    ));exit();
    }else if(strlen($gstin) > 15){
      echo json_encode(array(
          "message" => "GSTIN length exceeded more than 15 character !."
    ));exit();
    }else if(strlen($bill_pkey) > 18){
      echo json_encode(array(
          "message" => "Bill Pkey length exceeded more than 18 character !."
    ));exit();
    }else if(strlen($ce_flag) > 2){
      echo json_encode(array(
          "message" => "CE Flag length exceeded more than 2 character !."
    ));exit();
    }else if(strlen($doc_type) > 10){
      echo json_encode(array(
          "message" => "Doc Type length exceeded more than 10 character !."
    ));exit();
    }



	$database = new Database();
	$db = $database->getConnection();

	$list = new Company($db);
	$list->api_date = $postedData->api_date;
	$list->api_time = $postedData->api_time;
	$list->company_name = $postedData->company_name;
	$list->plant = $postedData->plant;
	$list->gstin = $postedData->gstin;
	$list->bill_pkey = $postedData->bill_pkey;
	$list->ce_flag = $postedData->ce_flag;
	$list->doc_type = $postedData->doc_type;

	if(
      !empty($list->api_date) &&
      !empty($list->api_time) &&
      !empty($list->company_name) &&
      !empty($list->plant) &&
      !empty($list->gstin) &&
      !empty($list->bill_pkey) &&
      !empty($list->ce_flag) &&
      !empty($list->doc_type) &&
      $list->create()
    ){
        $last_id = $db->lastInsertId();
        $list->id = $last_id;

        echo json_encode(
          array(
              "message" => "Record was created.",
              "list" => $list,
          ));
    }else{
        echo json_encode(array(
          "message" => "Unable to create record."
        ));exit();
    }


}else{
  	echo json_encode(array(
    	"message" => "Missing Required parameter."
    ));exit();
}

?>